from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import TodoListForm  # Import the TodoListForm
from .forms import TodoItemForm  # Import the TodoItemForm
# Create your views here.

def todo_list_view(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list.html', {'todo_lists': todo_lists})


def todo_list_detail_view(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)
    return render(request, 'todos/todo_list_detail.html', {'todo_list': todo_list})

from .forms import TodoListForm  # Assuming you have a form for TodoList

def todo_list_create_view(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect('todos:todo_list_detail', id=todo_list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/todo_list_create.html', {'form': form})

def todo_list_update_view(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)
    if request.method == 'POST':
        form = TodoListForm(request.POST, instance=todo_list)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_detail', id=id)
    else:
        form = TodoListForm(instance=todo_list)
    return render(request, 'todos/todo_list_update.html', {'form': form})


def todo_list_delete_view(request, id):
    todo_list = get_object_or_404(TodoList, pk=id)
    if request.method == 'POST':
        todo_list.delete()
        return redirect('todos:todo_list_list')  # Redirect to the list view after deletion
    return render(request, 'todos/todo_list_delete.html', {'todo_list': todo_list})


def todo_item_create_view(request):
    if request.method == 'POST':
        form = TodoItemForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_list')  # Redirect to the list view after creating a new item
    else:
        form = TodoItemForm()
    return render(request, 'todos/todo_item_create.html', {'form': form})

def todo_item_update_view(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == 'POST':
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect('todos:todo_list_detail', id=todo_item.list.id)  # Redirect to the detail view after updating
    else:
        form = TodoItemForm(instance=todo_item)
    return render(request, 'todos/todo_item_update.html', {'form': form})
